<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProcessController extends Controller
{
    private $costumers;
    private $salesman;
    private $sales;
    private $salesmanSalarySum;
    private $salesItemTotal;
    private $salesmanCounters;
    private $exportData;

    public function index()
    {
        $directory = getenv("HOMEDRIVE") . getenv('HOMEPATH') . '\data\in';

        if(!File::isDirectory($directory))
            throw new \Exception('Directory ' . $directory . ' does not exist');

        $fileList = File::files($directory);

        foreach ($fileList as $file) {
            $this->costumers = [];
            $this->salesman = [];
            $this->sales = [];
            $this->salesmanSalarySum = 0;
            $this->salesItemTotal = [];
            $this->salesmanCounters = [];
            $this->exportData = "";

            if (File::extension($file) == 'dat') {
                $fileProcessed = $this->processFile($file);

                if ($fileProcessed)
                    $this->createResultFile($file);
            }
        }
    }

    private function processFile($file)
    {
        try {
            $oFile = $file->openFile();

            while (!$oFile->eof()) {
                $lineContent = trim(preg_replace('/\s+/', ' ', $oFile->fgets()));
                $this->processFileLine($lineContent);
            }
            return true;
        } catch (\Exception $ex) {
            throw new \Exception('Cannot read file, reason:' . $ex->getMessage());
        }
    }

    private function processFileLine($lineContent)
    {
        $lineFields = explode(',', $lineContent);
        $lineType = $lineFields[0];

        switch ($lineType) {
            case '001':
                $cpf = $lineFields[1];
                $name = $lineFields[2];
                $salary = floatval($lineFields[3]);

                array_push($this->salesman, (object)[
                    "CPF" => $cpf,
                    "Name" => $name,
                    "Salary" => $salary,
                ]);

                $this->salesmanSalarySum += $salary;

                array_push($this->salesmanCounters, (object)[
                   "Name" => $name,
                    "TotalSales" => 0
                ]);

                break;
            case '002':
                $cpf = $lineFields[1];
                $name = $lineFields[2];
                $businessArea = $lineFields[3];

                array_push($this->costumers, (object)[
                    "CPF" => $cpf,
                    "Name" => $name,
                    "BusinessArea" => $businessArea,
                ]);
                break;
            case '003':
                $saleId = $lineFields[1];
                $salesmanId = end($lineFields);

                $itemString = string($lineContent)->between("[", "]");
                $itemArray = explode(",", $itemString);

                $item = (object)[
                    "ItemID" => $itemArray[0],
                    "ItemQuantity" => floatval(preg_replace("/[^-0-9\.]/", "", $itemArray[1])),
                    "ItemPrice" => floatval(preg_replace("/[^-0-9\.]/", "", $itemArray[2]))
                ];

                $totalItem = ($item->ItemPrice * $item->ItemQuantity);
                array_push($this->salesItemTotal, (object)[
                    "SaleID" => $saleId,
                    "TotalItem" => $totalItem
                ]);



//                $salesmanCounter = array_filter(
//                    $this->salesmanCounters,
//                    function ($e) use (&$salesmanId) {
//                        return $e->Name == $salesmanId;
//                    }
//                );
//                $salesmanCounter[0]->TotalSales += $totalItem;
//            var_dump(array_search($salesmanId, $this->salesmanCounters));
//                array_push($this->sales, (object)[
//                    "SaleID" => $saleId,
//                    "Item" => $item,
//                    "SalesmanID" => $salesmanId,
//                ]);
                break;
        }

    }

    private function createResultFile($file)
    {
//        dd($this->salesmanCounters);
        $filename = explode(".", $file->getFilename())[0];

        list($costumersQuantity, $salesmanQuantity, $salesmanSalaryAverage, $saleIdMaxValue) = $this->generateTotals();
        $this->generateData($costumersQuantity, $salesmanQuantity, $salesmanSalaryAverage, $saleIdMaxValue);

        Storage::disk('exports')->put($filename . '.done.dat', $this->exportData);
    }

    private function generateTotals(): array
    {
        $costumersQuantity = count($this->costumers);
        $salesmanQuantity = count($this->salesman);
        $salesmanSalaryAverage = round(($this->salesmanSalarySum / count($this->salesman)), 2);

        usort($this->salesItemTotal, function ($a, $b) {
            return strcmp($b->TotalItem, $a->TotalItem);
        });

        $saleIdMaxValue = $this->salesItemTotal[0]->SaleID;
        return array($costumersQuantity, $salesmanQuantity, $salesmanSalaryAverage, $saleIdMaxValue);
    }

    private function generateData($costumersQuantity, $salesmanQuantity, $salesmanSalaryAverage, $saleIdMaxValue): void
    {
        $this->exportData .= "Costumer Quantity:    " . $costumersQuantity . "\r\n";
        $this->exportData .= "Salesman Quantity:    " . $salesmanQuantity . "\r\n";
        $this->exportData .= "Salesman Average Salary:    " . $salesmanSalaryAverage . "\r\n";
        $this->exportData .= "Best Sale ID:    " . $saleIdMaxValue . "\r\n";
    }
}
